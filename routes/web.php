<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/handle-auth', [\App\Http\Controllers\VkAuthController::class, 'handleAuth']);
Route::get('/auth', [\App\Http\Controllers\VkAuthController::class, 'redirect']);
Route::get('/test', [\App\Http\Controllers\VkAuthController::class, 'test']);

Route::get('/', function () {
    return view('welcome');
});
