<?php

namespace App\Providers;

use App\Services\VkList\VkEntityListFetcher;
use App\Services\VkList\VkEntityListFetcherInterface;
use Illuminate\Support\ServiceProvider;

class VkEntityListServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(VkEntityListFetcherInterface::class, VkEntityListFetcher::class);
    }
}
