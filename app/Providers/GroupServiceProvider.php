<?php

namespace App\Providers;

use App\Services\GroupService;
use App\Services\GroupServiceInterface;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class GroupServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GroupServiceInterface::class, GroupService::class);
        $this->app->when(GroupService::class)->needs('$accessToken')->give('8e4b346470441ab9b50f82d6e401b028ccf9ef942d60ce77136507dcd939c872b6d3463a828ff9858b77e');
        $this->app->when(UserService::class)->needs('$accessToken')->give('8e4b346470441ab9b50f82d6e401b028ccf9ef942d60ce77136507dcd939c872b6d3463a828ff9858b77e');
    }
}
