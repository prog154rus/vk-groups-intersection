<?php

namespace App\Http\Controllers;

use App\Services\GroupServiceInterface;
use App\Services\UserService;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

//use Laravel\Socialite\Facades\Socialite;

class VkAuthController extends Controller
{
    private $groupService;
    private $userService;

    public function __construct(GroupServiceInterface $groupService, UserService $userService)
    {
        $this->groupService = $groupService;
        $this->userService = $userService;
    }

    public function redirect(Request $request) {
        return Socialite::with('vkontakte')->redirect();
    }

    public function handleAuth(Request $request) {
        $user = Socialite::driver('vkontakte')->user();
        dd($user);
    }

    public function test(Request $request) {
        return $this->groupService->getGroupMembersFiltered('tnull', ["sex" => 1]);
//        return $this->userService->getAllUserGroups();
    }
}
