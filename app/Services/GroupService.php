<?php
/**
 * Created by PhpStorm.
 * User: prog1
 * Date: 6/27/2018
 * Time: 10:58 PM
 */

namespace App\Services;


use App\Services\VkList\VkEntityListFetcherInterface;
use Illuminate\Support\Collection;
use VK\Client\VKApiClient;
use VK\Exceptions\Api\VKApiTooManyException;

class GroupService implements GroupServiceInterface
{

    private $vkApiClient;
    private $accessToken;
    private $vkFetcher;

    public function __construct(VKApiClient $vkClient, string $accessToken, VkEntityListFetcherInterface $vkFetcher)
    {
        $this->vkApiClient = $vkClient;
        $this->accessToken = $accessToken;
        $this->vkFetcher = $vkFetcher;
    }

    /**
     * @param string $groupIdOrName
     * @param array $filter
     * @return Collection
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    public function getGroupMembersFiltered(string $groupIdOrName, array $filter): Collection
    {
        $ageFrom = $filter["min_age"] ?? null;
        $ageTo = $filter["max_age"] ?? null;
        $sex = $filter["sex"] ?? null;
        $city = $filter["city"] ?? null;

        $groupId = $this->getGroupId($groupIdOrName);

        $searchUserParams = [
            "city" => $city,
            "age_from" => $ageFrom,
            "age_to" => $ageTo,
            "sex" => $sex,
            "group_id" => $groupId
        ];

        $vkClient = $this->vkApiClient;
        $accessToken = $this->accessToken;

        $resultUserList = $this->vkFetcher->fetchAll(
            function ($params) use ($vkClient, $accessToken){
                return $vkClient->users()->search($accessToken, $params);
            },
            $searchUserParams
        );

        return $resultUserList;
    }

    public function getGroupTags(string $groupId): Collection
    {
        // TODO: Implement getGroupTags() method.
    }

    /**
     * @param string $groupIdOrName
     * @return int
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    public function getGroupId(string $groupIdOrName): int
    {
        $requestParams = [
            "group_id" => $groupIdOrName
        ];
        $responseGroupInfo = $this->vkApiClient->groups()->getById($this->accessToken, $requestParams);

        if (count($responseGroupInfo) === 1) {
            return $responseGroupInfo[0]["id"];
        }
    }
}