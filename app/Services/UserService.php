<?php
/**
 * Created by PhpStorm.
 * User: prog1
 * Date: 6/28/2018
 * Time: 10:49 PM
 */

namespace App\Services;


use App\Services\VkList\VkEntityListFetcherInterface;
use Hamcrest\Thingy;
use VK\Client\VKApiClient;

class UserService
{

    private $vkApiClient;
    private $accessToken;
    private $vkEntityFetcher;

    public function __construct(VKApiClient $vkClient, string $accessToken, VkEntityListFetcherInterface $vkEntityFetcher)
    {
        $this->vkApiClient = $vkClient;
        $this->accessToken = $accessToken;
        $this->vkEntityFetcher = $vkEntityFetcher;
    }

    public function getAllUserGroups()
    {
        $vkClient = $this->vkApiClient;
        $accessToken = $this->accessToken;

        return $this->vkEntityFetcher->fetchAll(
            function ($params) use ($vkClient, $accessToken){
                return $vkClient->groups()->get($accessToken, $params);
            }, []);
    }
}