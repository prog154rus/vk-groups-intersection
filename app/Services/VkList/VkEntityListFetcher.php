<?php
/**
 * Created by PhpStorm.
 * User: prog1
 * Date: 6/28/2018
 * Time: 11:04 PM
 */

namespace App\Services\VkList;


use Illuminate\Support\Collection;
use VK\Exceptions\Api\VKApiTooManyException;

class VkEntityListFetcher implements VkEntityListFetcherInterface
{
    public function fetchAll(callable $callback, array $params): Collection
    {
        $offset = 0;

        /** @var Collection $resultUserList */
        $resultUserList = collect([]);

        $requestParams = $params;
        $requestParams["count"] = 1000;

        do {
            try {
                $requestParams ["offset"] = $offset;

                $response = call_user_func($callback, [$params]);

                $items = $response["items"];
                $resultUserList = $resultUserList->concat($items);

                $totalCount = $response["count"];

                $offset += 1000;
            } catch (VKApiTooManyException $ex) {
                sleep(1);
            }
        } while ($offset < $totalCount && count($items) == 1000);

        return $resultUserList;
    }
}