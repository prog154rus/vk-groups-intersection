<?php
/**
 * Created by PhpStorm.
 * User: prog1
 * Date: 6/28/2018
 * Time: 11:49 PM
 */

namespace App\Services\VkList;

use Illuminate\Support\Collection;

interface VkEntityListFetcherInterface
{
    public function fetchAll(callable $callback, array $params): Collection;
}