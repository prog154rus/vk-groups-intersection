<?php
/**
 * Created by PhpStorm.
 * User: prog1
 * Date: 6/27/2018
 * Time: 10:57 PM
 */

namespace App\Services;


use Illuminate\Support\Collection;

interface GroupServiceInterface
{
    public function getGroupMembersFiltered(string $groupId, array $filter): Collection;

    public function getGroupTags(string $groupId): Collection;

    public function getGroupId(string $groupIdOrName) : int;
}